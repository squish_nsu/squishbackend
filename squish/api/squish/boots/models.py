from django.db import models
from django.utils.translation import ugettext_lazy as _


class RentalPoint(models.Model):
    name = models.CharField(_("Short place name"), max_length=100, unique=True)

    address = models.CharField(_("Complete address"), max_length=200, unique=True)

    def __str__(self):
        return self.name


class StorageBox(models.Model):
    rental_point = models.ForeignKey(RentalPoint, related_name='boxes', on_delete=models.CASCADE)
    number = models.CharField(_("Box number"), max_length=100)

    def __str__(self):
        return f'{self.rental_point} #{self.number}'

    class Meta:
        unique_together = ('rental_point', 'number')


class BootPair(models.Model):
    size = models.CharField(_("Boot size"), max_length=16)

    def __str__(self):
        return f'Boot pair #{self.id} ({self.size})'


class BootLocation(models.Model):
    boot_pair = models.ForeignKey(BootPair, related_name='boxes', on_delete=models.CASCADE)
    box = models.ForeignKey(StorageBox, related_name='boots', on_delete=models.CASCADE)

    deleted = models.BooleanField(_("Deleted"), default=False)

    def __str__(self):
        return f'{self.boot_pair} in {self.box}'


class Order(models.Model):
    CREATED = 'created'
    STARTED = 'started'
    ENDED = 'ended'
    PAID = 'paid'

    STATUSES = (
        (CREATED, 'Created'),
        (STARTED, 'Started'),
        (ENDED, 'Ended'),
        (PAID, 'Paid'),
    )

    user = models.ForeignKey('accounts.User', related_name='orders', on_delete=models.CASCADE)
    boot_pair = models.ForeignKey(BootPair, related_name='orders', on_delete=models.CASCADE)
    status = models.CharField(_('Status'), max_length=100, choices=STATUSES, default=CREATED)

    box_started = models.ForeignKey(StorageBox, related_name='order_starts', on_delete=models.CASCADE)
    box_ended = models.ForeignKey(StorageBox, related_name='order_ends', blank=True, null=True, on_delete=models.CASCADE)

    date_started = models.DateTimeField(_('Date started'), blank=True, null=True)
    date_ended = models.DateTimeField(_('Date ended'), blank=True, null=True)

    @property
    def time_amount(self):
        if not self.date_started or not self.date_ended:
            return None

        # Every minute is rated 3 rubles
        RATE = 3
        minutes_passed = int((self.date_ended - self.date_started).total_seconds()) // 60 + 1
        if minutes_passed <= 10:
            return 0
        return (minutes_passed - 10) * RATE

    @property
    def paid_amount(self):
        paid = Transaction.objects.filter(order=self).aggregate(paid=models.Sum('amount'))

        if not paid:
            return 0

        return paid['paid']

    def __str__(self):
        return f'Order from {self.user} for {self.boot_pair} ({self.date_started or ""} - {self.date_ended or ""})'

    class Meta:
        ordering = ('-date_started', )


class Transaction(models.Model):
    order = models.ForeignKey(Order, related_name='transactions', on_delete=models.CASCADE)
    amount = models.IntegerField(_('Amount'))

    def __str__(self):
        return f'{self.amount} for order #{self.order.id}'
