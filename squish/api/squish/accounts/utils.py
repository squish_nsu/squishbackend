import uuid
from random import randint


def generate_confirmation_code():
    return str(randint(1000, 9999))


def generate_password_recovery_code():
    return str(uuid.uuid4())[-8:]
