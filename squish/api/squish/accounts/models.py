from django.contrib.auth.models import (AbstractBaseUser, BaseUserManager,
                                        PermissionsMixin)
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _


class UserManager(BaseUserManager):
    def create_user(self, phone, password, **kwargs):
        user = self.model(phone=phone, **kwargs)

        user.set_password(password)
        user.save()

        return user

    def create_superuser(self, phone, password, **kwargs):
        return self.create_user(
            phone,
            password,
            is_staff=True,
            is_active=True,
            is_superuser=True,
            **kwargs
        )

    def get_by_natural_key(self, username):
        return self.get(phone__iexact=username)


class User(PermissionsMixin, AbstractBaseUser):
    phone = models.CharField(_("Phone number"), max_length=100, unique=True)

    unconfirmed_phone = models.CharField(_("Unconfirmed phone address"), max_length=100, blank=True, null=True)
    confirmation_code = models.CharField(_("Confirmation code"), max_length=100, blank=True, null=True)

    password_recovery_code = models.CharField(_("Password recovery code"), max_length=100, blank=True, null=True)

    is_staff = models.BooleanField(_("Admin"), default=False)
    is_active = models.BooleanField(_("Active user"), default=False)

    date_joined = models.DateTimeField(_("Date joined"), default=timezone.now, blank=True, null=True)

    objects = UserManager()

    USERNAME_FIELD = 'phone'

    def __str__(self):
        return self.phone
