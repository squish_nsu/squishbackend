from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.decorators import action
from rest_framework.mixins import RetrieveModelMixin, UpdateModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.validators import ValidationError
from rest_framework.viewsets import GenericViewSet
from rest_framework_simplejwt.tokens import RefreshToken

from accounts import models as m
from accounts import serializers as s
from accounts.permissions import IsUserOrReadOnly
from accounts.tasks import (confirm_phone, confirm_registration,
                            recover_password)
from accounts.utils import generate_confirmation_code

phone = openapi.Parameter('phone', openapi.IN_QUERY, description="Phone number",
                          type=openapi.TYPE_STRING, required=True)

user_id_schema = openapi.Schema(title="User ID", type=openapi.TYPE_INTEGER)
phone_schema = openapi.Schema(title='Phone number', type=openapi.TYPE_STRING)
access_token_schema = openapi.Schema(title="Access token", type=openapi.TYPE_STRING)
refresh_token_schema = openapi.Schema(title="Refresh token", type=openapi.TYPE_STRING)
status_schema = openapi.Schema(title='Status', type=openapi.TYPE_STRING, enum=['available', 'registered'])


user_create_response = openapi.Response(
    description='',
    schema=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        properties={
            'userId': user_id_schema,
            'phone': phone_schema
        },
        required=['userId', 'phone']
    )
)

user_check_registered_response = openapi.Response(
    description='',
    schema=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        properties={
            'status': status_schema,
            'message': openapi.Schema(title='Message', type=openapi.TYPE_STRING),
        },
        required=['status', 'message']
    )
)

user_tokens_response = openapi.Response(
    description='',
    schema=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        properties={
            'userId': user_id_schema,
            'access': access_token_schema,
            'refresh': refresh_token_schema
        },
        required=['userId', 'access', 'refresh']
    )
)


class UserRegisterViewSet(GenericViewSet):
    """
    User registration

    create: Creates user with `phone`, sends notification with confirmation code via phone.

    confirm: Using `phone` and `confirmation_code`, finishes registration and returns auth token.

    resend: Resend confirmation code via phone.

    check_registered: Endpoint, to check if phone number is available. Query-parameter `phone` is required.
    Return object with keys `{status, message}`, where status can be `available` or `registered`.
    """
    permission_classes = []

    def get_queryset(self):
        return m.User.objects.all()

    def get_serializer_class(self):
        if self.action == 'confirm':
            return s.UserConfirmPhoneSerializer

        if self.action == 'resend':
            return s.UserResendCodeSerializer

        else:
            return s.UserRegisterSerializer

    def try_get_unconfirmed_user(self, phone):
        try:
            return m.User.objects.filter(confirmation_code__isnull=False).get(phone__iexact=phone, unconfirmed_phone__iexact=phone)
        except m.User.DoesNotExist:
            pass

    @swagger_auto_schema(responses={200: user_create_response})
    def create(self, request, *args):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        data = serializer.validated_data
        phone = data.get('phone')

        kwargs = {
            'is_active': True,
            'unconfirmed_phone': phone,
            'confirmation_code': generate_confirmation_code()
        }

        user = self.try_get_unconfirmed_user(phone)

        if user:
            raise ValidationError({
                'phone': [_('User must confirm this phone number.')]
            })
        else:
            user = m.User.objects.create_user(phone, data['password'], **kwargs)

        confirm_registration(user, user.confirmation_code)

        response = Response({
            'user_id': user.id,
            'phone': phone
        })

        return response

    @swagger_auto_schema(responses={200: user_tokens_response})
    @action(detail=False, methods=['POST'])
    def confirm(self, request, *args):
        serializer = s.UserConfirmPhoneSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        phone = serializer.validated_data.get('phone')
        code = serializer.validated_data.get('code')

        try:
            user = m.User.objects.get(unconfirmed_phone__iexact=phone, confirmation_code=code)
        except m.User.DoesNotExist:
            return Response({
                'non_field_errors': [_("Activation code is invalid.")],
                "status": 1002
            }, status=400)

        user.phone = user.unconfirmed_phone
        user.unconfirmed_phone = None
        user.confirmation_code = None
        user.save()

        refresh = RefreshToken.for_user(user)
        user.last_login = timezone.now()
        user.save()

        response = Response({
            'user_id': user.id,
            'refresh': str(refresh),
            'access': str(refresh.access_token),
        })

        return response

    @action(detail=False, methods=['POST'])
    def resend(self, request, *args):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        data = serializer.validated_data
        phone = data.get('phone')

        try:
            user = m.User.objects.get(unconfirmed_phone=phone)
        except m.User.DoesNotExist:
            return Response({}, status=200)

        user.confirmation_code = generate_confirmation_code()
        user.save()

        confirm_registration(user, user.confirmation_code)

        return Response({'phone': phone})

    @swagger_auto_schema(manual_parameters=[phone], responses={200: user_check_registered_response})
    @action(detail=False, methods=['GET'])
    def check_registered(self, request, *args):
        phone = request.query_params.get('phone')

        if not phone:
            raise ValidationError({'non_field_errors': [_('Either `phone` field is required.')]})

        try:
            queryset = m.User.objects.filter(confirmation_code__isnull=True)
            queryset.get(phone__iexact=phone)

            message = _('An account with this phone number already exists. Please try different one.')

            return Response({'status': 'registered', 'message': message})
        except m.User.DoesNotExist:
            return Response({'status': 'available', 'message': _('Phone number is available for registration.')})


class UserViewSet(RetrieveModelMixin, UpdateModelMixin, GenericViewSet):
    """
    update:
    Update user info.

    Does not update `password`, please use special methods to password.

    If `phone` is changed, sends notification with confirmation code via phone.
    User must confirm their phone number with a `confirmation_code`, as when finishing registration.

    update_password:
    Change password.

    Accepts new and old password.
    """
    queryset = m.User.objects.all()
    serializer_class = s.UserEditSerializer
    permission_classes = [IsAuthenticated, IsUserOrReadOnly]

    def get_serializer_class(self):
        if self.action == 'update_password':
            return s.UserChangePasswordSerializer

        return self.serializer_class

    def perform_update(self, serializer):
        user = self.get_object()
        new_phone = serializer.validated_data.get('phone')

        kwargs = {}
        if new_phone and user.phone != new_phone:
            kwargs['phone'] = user.phone
            kwargs['unconfirmed_phone'] = new_phone
            confirmation_code = generate_confirmation_code()
            kwargs['confirmation_code'] = confirmation_code

            confirm_phone(new_phone, confirmation_code)

        serializer.save(**kwargs)

    @swagger_auto_schema(responses={200: user_tokens_response})
    @action(detail=False, methods=['PUT'])
    def update_password(self, request, *args):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        user = request.user

        user.set_password(serializer.validated_data.get('password'))
        refresh = RefreshToken.for_user(user)
        user.last_login = timezone.now()
        user.save()

        response = Response({
            'user_id': user.id,
            'refresh': str(refresh),
            'access': str(refresh.access_token),
        }, status=200)

        return response


password_restore_response = openapi.Response(
    description='',
    schema=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        properties={
            'phone': phone_schema
        },
        required=['phone']
    )
)


class UserRestorePasswordViewSet(GenericViewSet):
    """
    create:
    Initialize restore password process.

    Sends SMS with password recovery code.

    restore:
    Complete restore password process.

    Enpoint accepts phone, restore code and new password.

    If code is correct, new password is set and user must login using their phone number and password.
    """
    queryset = m.User.objects.all()
    permission_classes = []
    serializer_class = s.UserRestorePasswordSerializer

    def get_serializer_class(self):
        if self.action == 'restore':
            return s.UserUpdatePasswordSerializer

        return self.serializer_class

    def create(self, request, *args):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        data = serializer.validated_data
        phone = data.get('phone')

        try:
            user = m.User.objects.get(phone__iexact=phone)
        except m.User.DoesNotExist:
            return Response({}, status=200)

        user.password_recovery_code = generate_confirmation_code()
        user.save()

        recover_password(user, user.password_recovery_code)

        return Response({'phone': phone}, status=201)

    @swagger_auto_schema(responses={200: password_restore_response})
    @action(detail=False, methods=['PUT'])
    def restore(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        phone = serializer.validated_data.get('phone')
        code = serializer.validated_data.get('code')
        password = serializer.validated_data.get('password')

        try:
            user = m.User.objects.get(phone__iexact=phone, password_recovery_code=code)
        except m.User.DoesNotExist:
            return Response({'non_field_errors': [_('Restore password link is invalid.')]}, status=400)

        user.set_password(password)
        user.password_recovery_code = None
        user.save()

        return Response({'phone': user.phone})
