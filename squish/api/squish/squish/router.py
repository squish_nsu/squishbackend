from rest_framework import routers

from accounts.router import register as register_accounts
from boots.router import register as register_boots

router = routers.DefaultRouter()

register_accounts(router)
register_boots(router)
