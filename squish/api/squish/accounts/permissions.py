from django.utils.translation import ugettext_lazy as _

from rest_framework.exceptions import ValidationError
from rest_framework.permissions import SAFE_METHODS, BasePermission


class IsUserConfirmed(BasePermission):
    message = _("You must confirm your email.")

    def has_permission(self, request, view):
        if not request.user or not request.user.pk:
            return False

        if request.user.email == request.user.unconfirmed_email:
            raise ValidationError({'non_field_errors': [self.message], 'status': 1001})

        return True


class IsUserOrReadOnly(BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS:
            return True

        return obj.id == request.user.id
