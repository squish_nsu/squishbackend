from django.contrib import admin

from boots import models as m

admin.site.register(m.RentalPoint)
admin.site.register(m.StorageBox)
admin.site.register(m.BootPair)
admin.site.register(m.BootLocation)


@admin.register(m.Order)
class OrderAdmin(admin.ModelAdmin):
    list_filter = ('status', )
