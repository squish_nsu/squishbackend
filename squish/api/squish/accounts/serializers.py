from django.core.validators import MinLengthValidator
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers, validators

from accounts.models import User


class UserRegisterSerializer(serializers.Serializer):
    phone = serializers.CharField(
        validators=[
            validators.UniqueValidator(
                queryset=User.objects.filter(confirmation_code__isnull=True),
                message=_('This phone number is already registered.'),
                lookup='iexact'
            )
        ],
    )

    password = serializers.CharField(write_only=True, validators=[MinLengthValidator(6, _('Password must be at least 6 charactes long.'))])


class UserConfirmPhoneSerializer(serializers.Serializer):
    phone = serializers.CharField()
    code = serializers.CharField(write_only=True)


class UserResendCodeSerializer(serializers.Serializer):
    phone = serializers.CharField()


class UserEditSerializer(serializers.ModelSerializer):
    phone = serializers.CharField(required=False, allow_blank=True)

    class Meta:
        model = User
        fields = ('id', 'phone',)

    def validate_phone(self, value):
        request = self.context.get('request')

        try:
            user = User.objects.get(phone__iexact=value)
        except User.DoesNotExist:
            user = None

        if user and user != request.user:
            raise serializers.ValidationError(_('This phone number is already registered.'))

        return value


class UserChangePasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField(write_only=True)
    password = serializers.CharField(write_only=True, validators=[MinLengthValidator(6, _('Password must be at least 6 charactes long.'))])
    password_confirmation = serializers.CharField(
        write_only=True,
        validators=[MinLengthValidator(6, _('Password confirmation must be at least 6 charactes long.'))]
    )

    def validate_old_password(self, value):
        request = self.context.get('request', None)
        if not request:
            raise serializers.ValidationError(_('Request was not provided for this serializer.'))

        if not request.user.check_password(value):
            raise serializers.ValidationError(_('Wrong password for user.'))

        return value

    def validate(self, data):
        password = data['password']
        password_confirmation = data['password_confirmation']

        if password and password_confirmation and password != password_confirmation:
            raise serializers.ValidationError(_('The two password fields didn’t match.'))

        return data


class UserRestorePasswordSerializer(serializers.Serializer):
    phone = serializers.CharField()


class UserUpdatePasswordSerializer(UserConfirmPhoneSerializer):
    password = serializers.CharField(write_only=True, validators=[MinLengthValidator(6, _('Password must be at least 6 charactes long.'))])
