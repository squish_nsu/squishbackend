from accounts import viewsets as v


def register(router):
    router.register(r'registration', v.UserRegisterViewSet, basename='registration')
    router.register(r'restore-password', v.UserRestorePasswordViewSet, basename='restore_password')
    router.register(r'accounts', v.UserViewSet, basename='users')
