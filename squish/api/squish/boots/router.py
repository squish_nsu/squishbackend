from . import viewsets as v


def register(router):
    router.register(r'rental_points', v.RentalPointViewSet, basename='rental_points')
    router.register(r'orders', v.OrderViewSet, basename='orders')
