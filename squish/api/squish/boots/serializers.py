from rest_framework import serializers

from . import models as m


class StorageBoxSerializer(serializers.ModelSerializer):
    class Meta:
        model = m.StorageBox
        fields = ('id', 'number', )


class BootPairSerializer(serializers.ModelSerializer):
    class Meta:
        model = m.BootPair
        fields = ('id', 'size')
        read_only_fields = ('id', 'size')


class RentalPointSerializer(serializers.ModelSerializer):
    sizes = serializers.SerializerMethodField()
    free_boxes = serializers.SerializerMethodField()

    class Meta:
        model = m.RentalPoint
        fields = ('id', 'name', 'address', 'sizes', 'free_boxes')

    def get_sizes(self, obj):
        qs = m.BootLocation.objects.filter(box__rental_point=obj, deleted=False).values('boot_pair__size').distinct()

        return [x['boot_pair__size'] for x in qs]

    def get_free_boxes(self, obj):
        qs = m.StorageBox.objects.filter(rental_point=obj).exclude(boots__deleted=False)

        return StorageBoxSerializer(qs, many=True, context=self.context).data


class OrderSerializer(serializers.ModelSerializer):
    boot_pair = BootPairSerializer(read_only=True)
    box_started = StorageBoxSerializer(read_only=True)
    box_ended = StorageBoxSerializer(read_only=True)

    class Meta:
        model = m.Order
        fields = (
            'id',
            'boot_pair',
            'status',
            'box_started',
            'box_ended',
            'date_started',
            'date_ended',
            'time_amount',
            'paid_amount',
        )

        read_only_fields = (
            'id',
            'boot_pair',
            'status',
            'box_started',
            'box_ended',
            'date_started',
            'date_ended',
            'paid_amount',
        )


class OrderCreateSerializer(serializers.Serializer):
    size = serializers.CharField()
    rental_point = serializers.IntegerField()

    def validate_rental_point(self, value):
        try:
            return m.RentalPoint.objects.get(pk=value)
        except m.RentalPoint.DoesNotExist:
            raise serializers.ValidationError('Rental point was not found.')


class OrderEndSerializer(serializers.Serializer):
    box = serializers.IntegerField()

    def validate_box(self, value):
        try:
            return m.StorageBox.objects.exclude(boots__deleted=False).get(pk=value)
        except m.StorageBox.DoesNotExist:
            raise serializers.ValidationError('Box is not free.')


class OrderTransactionSerializer(serializers.Serializer):
    amount = serializers.IntegerField()

    def validate_amount(self, value):
        if value <= 0:
            raise serializers.ValidationError('Amount must be positive integer')

        return value
