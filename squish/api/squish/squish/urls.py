from django.conf import settings
from django.contrib import admin
from django.urls import include, path, re_path
from drf_yasg.utils import swagger_auto_schema

from rest_framework_simplejwt import views as jwt_views
from squish.router import router

from accounts.jwt.views import TokenObtainPairView, refresh_token_response

token_refresh_view = swagger_auto_schema(
    method='post',
    responses={200: refresh_token_response}
)(jwt_views.TokenRefreshView.as_view())


urlpatterns = [
    path('admin/', admin.site.urls),

    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', token_refresh_view, name='token_refresh'),

    re_path(r'^api/', include((router.urls, 'router'), namespace='api')),
]

if settings.ENABLE_DOCS:
    from rest_framework import permissions
    from drf_yasg.views import get_schema_view
    from drf_yasg import openapi

    schema_view = get_schema_view(
        openapi.Info(
            title="Squish API",

            default_version='api',
            description="Squish API",
        ),
        public=True,
        permission_classes=(permissions.AllowAny,),
    )

    urlpatterns += [
        path('docs/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    ]


if settings.DEBUG:
    from django.conf.urls.static import static
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
