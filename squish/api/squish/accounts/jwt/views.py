from django.utils.decorators import method_decorator

from accounts.jwt.serializers import TokenObtainPairSerializer
from accounts.viewsets import (access_token_schema, refresh_token_schema,
                               user_tokens_response)
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework_simplejwt.views import TokenViewBase

refresh_token_response = openapi.Response(
    description='',
    schema=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        properties={
            'access': access_token_schema,
            'refresh': refresh_token_schema
        },
        required=['access', 'refresh']
    )
)


@method_decorator(name='post', decorator=swagger_auto_schema(responses={200: user_tokens_response}))
class TokenObtainPairView(TokenViewBase):
    """
    Takes a set of user credentials and returns an access and refresh JSON web
    token pair to prove the authentication of those credentials.
    """
    serializer_class = TokenObtainPairSerializer
