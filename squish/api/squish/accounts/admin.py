from django.contrib import admin, auth
from django.utils.translation import ugettext_lazy as _

from accounts.models import User


@admin.register(User)
class UserAdmin(auth.admin.UserAdmin):
    fieldsets = (
        (None, {'fields': ('phone', 'password', )}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
        (_('Codes'), {'fields': ('unconfirmed_phone', 'confirmation_code', 'password_recovery_code')})
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('phone', 'password1', 'password2'),
        }),
    )

    readonly_fields = ('unconfirmed_phone', 'confirmation_code', 'password_recovery_code')
    list_display = ('phone', 'is_staff', 'date_joined', )
    list_filter = ('is_staff', 'is_superuser', 'is_active', )
    search_fields = ('phone', )
    ordering = ('-pk',)
