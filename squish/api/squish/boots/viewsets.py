from django.utils import timezone
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.mixins import (CreateModelMixin, ListModelMixin,
                                   RetrieveModelMixin)
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from . import models as m
from . import serializers as s


class RentalPointViewSet(ListModelMixin, RetrieveModelMixin, GenericViewSet):
    queryset = m.RentalPoint.objects.all()
    serializer_class = s.RentalPointSerializer


class OrderViewSet(ListModelMixin, CreateModelMixin, RetrieveModelMixin, GenericViewSet):
    permission_classes = (IsAuthenticated, )

    serializer_class = s.OrderSerializer

    def get_queryset(self):
        return m.Order.objects.filter(user=self.request.user)

    def create(self, request):
        serializer = s.OrderCreateSerializer(data=request.data, context=self.get_serializer_context())
        serializer.is_valid(raise_exception=True)
        size = serializer.validated_data['size']

        location = m.BootLocation.objects.filter(boot_pair__size=size, deleted=False).first()
        if not location:
            raise ValidationError({'non_field_errors': [f'No boots available with size "{size}".']})

        order = m.Order.objects.create(
            user=self.request.user,
            boot_pair=location.boot_pair,
            box_started=location.box,
            status=m.Order.CREATED
        )

        location.deleted = True
        location.save()

        serializer = self.get_serializer(order)

        return Response(serializer.data)

    @action(detail=True, methods=['POST'])
    def start(self, request, pk=None):
        order = self.get_object()

        if order.status != m.Order.CREATED:
            raise ValidationError({'non_field_errors': ['Cannot start order with status != \'created\'.']})

        order.status = m.Order.STARTED
        order.date_started = timezone.now()
        order.save()

        m.Transaction.objects.create(
            order=order,
            amount=30
        )

        serializer = self.get_serializer(order)

        return Response(serializer.data)

    @action(detail=True, methods=['POST'])
    def end(self, request, pk=None):
        serializer = s.OrderEndSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        order = self.get_object()

        if order.status != m.Order.STARTED:
            raise ValidationError({'non_field_errors': ['Cannot end order with status != \'started\'.']})

        order.status = m.Order.ENDED
        order.box_ended = serializer.validated_data['box']
        order.save()

        m.BootLocation.objects.create(
            boot_pair=order.boot_pair,
            box=order.box_ended
        )

        serializer = self.get_serializer(order)

        return Response(serializer.data)

    @action(detail=True, methods=['POST'])
    def pay(self, request, pk=None):
        order = self.get_object()

        if order.status != m.Order.ENDED:
            raise ValidationError({'non_field_errors': ['Cannot pay for order with status != \'ended\'.']})

        order.date_ended = timezone.now()
        order.status = m.Order.PAID
        order.save()

        m.Transaction.objects.create(order=order, amount=order.time_amount)

        serializer = self.get_serializer(order)

        return Response(serializer.data)
