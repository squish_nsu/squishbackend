import logging

import requests
from django.conf import settings
from django.template.loader import render_to_string

from django_rq import job

logger = logging.getLogger(__name__)


@job
def send_sms(phones, text):
    if not settings.ENABLE_SMS_SENDING:
        logger.info(f'Fake SMS to {phones}: {text}')
        return

    sms_settings = settings.SMS
    url = sms_settings.get('URL')
    login = sms_settings.get('LOGIN')
    password = sms_settings.get('PASSWORD')

    def to_str(number):
        if isinstance(number, str):
            return number
        else:
            return str(number)

    phones = ",".join(map(to_str, phones))

    params = {
        'login': login,
        'psw': password,
        'phones': phones,
        'mes': text,
        'charset': 'utf-8'
    }

    r = requests.post(url, params=params)

    logger.info('SMS sent to {}: {}.'.format(phones, r.text))


def confirm_registration(phone, confirmation_code):
    body = render_to_string('sms/registration.txt', context={'confirmation_code': confirmation_code})

    send_sms.delay([phone], body)


def confirm_phone(phone, confirmation_code):
    body = render_to_string('sms/registration.txt', context={'confirmation_code': confirmation_code})

    send_sms.delay([phone], body)


def recover_password(phone, recovery_code):
    body = render_to_string('sms/restore_password.txt', context={'recovery_code': recovery_code})

    send_sms.delay([phone], body)
